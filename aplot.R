#!/usr/bin/Rscript --silent

me <- "rsf.2016.001.ini-deep-output"
args <- commandArgs(trailingOnly = TRUE)
if ( length(args) > 0 ) {
	me <- args[1]
}
hbpattern <- paste0(me,".*hb$")
hblist <- list.files(path = ".", pattern = hbpattern, all.files = FALSE,full.names = FALSE, recursive = FALSE,ignore.case = FALSE, include.dirs = FALSE, no.. = TRUE)
hblist
tdata <- do.call(rbind, lapply(hblist, FUN=function(x) {q <- read.csv(x, header=F, sep=' ');return(q[,c(2,1,3,4)])}))
dose <- tdata$V1
dose[tdata$V1==0] <- 1
dose[tdata$V1==1] <- 0.5
dose[tdata$V1==2] <- 0.78
dose[tdata$V1==3] <- 1.4
dose[tdata$V1==4] <- 2
tdata <- cbind(tdata, dose)
tdata <- tdata[order(tdata$dose),]

miny <- min(tdata$V3, tdata$V4)
maxy <- max(tdata$V3, tdata$V4)

miny <- 35
maxy

pdf(file = paste0(me,"-borgraph.pdf"), width = 8, height = 4)
par(mfrow=c(1, 2))
plot (tdata[tdata$V2 == 42.975,]$dose, tdata[tdata$V2 == 42.975,]$V4, type = 'l', xlab = 'dose', ylab = 'interface position, %EL', ylim = c(miny, maxy))
lines(tdata[tdata$V2 == 49.225,]$dose, tdata[tdata$V2 == 49.225,]$V4, type = 'l', pch = 10, col = 'red')
lines(tdata[tdata$V2 == 55.475,]$dose, tdata[tdata$V2 == 55.475,]$V4, type = 'l', pch = 12, col = 'blue')
lines(tdata[tdata$V2 == 61.725,]$dose, tdata[tdata$V2 == 61.725,]$V4, type = 'l', pch = 14, col = 'green')
lines(tdata[tdata$V2 == 67.975,]$dose, tdata[tdata$V2 == 67.975,]$V4, type = 'l', pch = 16, col = 'brown')

points(tdata[tdata$V2 == 42.975,]$dose, tdata[tdata$V2 == 42.975,]$V3, pch = 1, col = 'black')
points(tdata[tdata$V2 == 49.225,]$dose, tdata[tdata$V2 == 49.225,]$V3, pch = 10, col = 'red')
points(tdata[tdata$V2 == 55.475,]$dose, tdata[tdata$V2 == 55.475,]$V3, pch = 12, col = 'blue')
points(tdata[tdata$V2 == 61.725,]$dose, tdata[tdata$V2 == 61.725,]$V3, pch = 14, col = 'green')
points(tdata[tdata$V2 == 67.975,]$dose, tdata[tdata$V2 == 67.975,]$V3, pch = 16, col = 'brown')

legend('bottomright', c('42.975', '49.225', '55.475', '61.725', '67.975'), lty = c(1, 1, 1, 1, 1), pch = c(1, 10, 12, 14, 16), col = c('black', 'red', 'blue', 'green', 'brown'))

plot (tdata[tdata$dose == 0.5,]$V2, tdata[tdata$dose == 0.5,]$V4, type = 'l', xlab = 'time, m', ylab = 'interface position, %EL', ylim = c(miny, maxy))
lines(tdata[tdata$dose == 0.78,]$V2, tdata[tdata$dose == 0.78,]$V4, type = 'l', pch = 10, col = 'red')
lines(tdata[tdata$dose == 1,]$V2, tdata[tdata$dose == 1,]$V4, type = 'l', pch = 12, col = 'blue')
lines(tdata[tdata$dose == 1.4,]$V2, tdata[tdata$dose == 1.4,]$V4, type = 'l', pch = 14, col='green')
lines(tdata[tdata$dose == 2,]$V2, tdata[tdata$dose == 2,]$V4, type = 'l', pch = 16, col = 'brown')

points(tdata[tdata$dose == 0.5,]$V2, tdata[tdata$dose == 0.5,]$V3, pch = 1, col = 'black')
points(tdata[tdata$dose == 0.78,]$V2, tdata[tdata$dose == 0.78,]$V3, pch = 10, col = 'red')
points(tdata[tdata$dose == 1,]$V2, tdata[tdata$dose == 1,]$V3, pch = 12, col = 'blue')
points(tdata[tdata$dose == 1.4,]$V2, tdata[tdata$dose == 1.4,]$V3, pch = 14, col='green')
points(tdata[tdata$dose == 2,]$V2, tdata[tdata$dose == 2,]$V3, pch = 16, col = 'brown')

legend(43, 65, c('0.5', '0.78'), xpd = TRUE, lty = c(1, 1), pch = c(1, 10), col = c('black', 'red'))
legend(53, 65, c('1', '1.4'), xpd = TRUE, lty = c(1, 1), pch = c(12, 14), col = c('blue', 'green'))
legend(62, 64, c('2'), xpd = TRUE, lty = c(1), pch = c(16), col = c('brown'))

q <- dev.off()

