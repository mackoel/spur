#!/usr/bin/Rscript --silent
rem <- function(...) invisible(T)
rem( '
Rscript.exe "%~F0" "%1" "%2" "%3" "%4" "%5" "%~F6" "%~F7","%~F8","%~F9"
EXIT /B
rem ')
### above=BAT, below=R
pkgrequire <- function(pkg, preferred = 'http://cran.rstudio.com/') {
	if (!require(pkg, character.only = TRUE)) {
		cat("Cannot find", pkg, "package.\n")
		lib_path = Sys.getenv("R_LIBS_USER")
		if (!file.exists(lib_path)) dir.create(lib_path, rec = TRUE)
		install.packages(pkg, dep = TRUE, repos = preferred, lib = lib_path)
		if (!require(pkg, character.only = TRUE, lib.loc = lib_path)) stop("Package not found")
	}
}
pkgrequire("optparse")
pkgrequire("RColorBrewer")
pkgrequire("reshape2")

option_list <- list(
	make_option(c("-v", "--verbose"), action="store_true", default=FALSE,
							help="Print extra output [default=false]"),
	make_option(c("-a", "--minint"), type="double", default = 10,
							help="Number of breaks for final ato pattern [default %default]"),
	make_option(c("-b", "--breaks"), type="integer", default = 50,
							help="Zero level for raw hex pattern [default %default]", metavar="number"),
	make_option(c("-c", "--comp"), type="character", default = "mrna",
							help="comp [default %default]"),
	make_option(c("-d", "--doff"), type="character", default = "00",
							help="doff [default %default]", metavar="number"),
	make_option(c("-e", "--knn"), type="integer", default = 2,
							help="Offset [default %default]", metavar="number"),
	make_option(c("-g", "--genes"), type="integer", default = 4,
							help="Number of genes [default %default]", metavar="number"),
	make_option(c("-l", "--lab"), type="character", default = "circle",
							help="lab [default %default]"),
	make_option(c("-k", "--imgbkg"), type="character", default = "",
							help="imgbkg [default empty %default]"),
	make_option(c("-m", "--mol"), type="integer", default = 0,
							help="number of molecules per conc [default %default]"),
	make_option(c("-n", "--nuc"), type="integer", default = 0,
							help="Nuc number [default %default]"),
	make_option(c("-r", "--rev"), type="integer", default = 10,
							help="Zero level for final ato pattern [default %default]"),
	make_option(c("-o", "--ordered"), action="store_true", default=FALSE,
							help="order by conc [default=false]"),
	make_option(c("-p", "--pdf"), action="store_true", default=FALSE,
							help="PDF [default=false]"),
	make_option(c("-s", "--smooth"), type="double", default = 2*10^-8,
							help="smooth [default %default]"),
	make_option(c("-i", "--x1"), type="integer", default = 3,
							help="Number of breaks for raw hex pattern [default %default]", metavar="number"),
	make_option(c("-j", "--y1"), type="integer", default = 60,
							help="Number of heXbins along X(D-V) axis [default %default]", metavar="number"),
	make_option(c("-t", "--tag"), type="character", default = "overlay",
							help="smooth [default %default]")
)
opt_parser <- OptionParser(usage = "usage: %prog [options]", option_list = option_list, description = "Plot cells filtered in different way", epilogue = "Send feedback to mackoel@gmail.com")
cmdline <- commandArgs(trailingOnly = TRUE)
flaggs <- parse_args(opt_parser, args = cmdline, print_help_and_exit = TRUE, positional_arguments = TRUE)
opts <- flaggs$options
args <- flaggs$args

inpfiles <- unlist(strsplit(args[1], ",", fixed = TRUE))
uoffile <- inpfiles[1]
logfile1 <- inpfiles[2]
outfiles <- unlist(strsplit(args[2], ",", fixed = TRUE))
resfile <-  outfiles[1]

pat <- read.csv(uoffile, sep = ' ', header =F)
head(pat)
time <- unique(pat$V4)
run <- unique(pat$V1)
run
time

vmax <- apply(pat, 2, max)
if (opts$mol > 0) {
	vmax <- rep(1, length(vmax))
}
vmax
npat <- pat

n_col_min <- 5
n_col_max <- n_col_min + opts$genes * 2 - 1

v <- do.call(cbind, lapply(n_col_min:n_col_max, FUN=function(n) {pat[,n]/vmax[n]}))

npat[,n_col_min:n_col_max] <- v
summary(npat)

palette <- colorRampPalette(c("grey", "yellow", "green"))
cols = palette(length(run))

gcols <- lapply(1:opts$genes, FUN=function(i) paste0('g', i))
mcols <- lapply(1:opts$genes, FUN=function(i) paste0('m', i))

gcols
mcols

dnpat <- data.frame(npat)
colnames(dnpat) <- c('run', 'index', 'nuc', 'time', gcols, mcols)
cat('dnpat\n')
summary(dnpat)

dm <- melt(dnpat, c('run', 'index', 'nuc', 'time'), c(gcols, mcols), 'spec')
head(dm)
summary(dm)

gmean <- do.call(cbind, split(dm$value, dm$run))
head(gmean)

gm <- rowMeans(gmean)
head(gm)

ddm <- dcast(dm, index + time + nuc ~ spec, mean)
cat('ddm\n')
head(ddm)

sddm <- dcast(dm, index + time + nuc ~ spec, sd)
cat('sddm\n')
head(sddm)

wdata <- read.csv('wdata8.short.csv', header = TRUE)
wmax <- apply(wdata, 2, max)
ylim_max <- 1
if (opts$mol > 0) {
	ylim_max <- max(wmax) * opts$mol
	wmax <- rep(1.0/opts$mol, length(wmax))
}
summary(wdata)

gColumn <- which(colnames(ddm) == 'g1') - 1
gColumn
mColumn <- which(colnames(ddm) == 'm1') - 1
mColumn

pdf(file=paste0(resfile, length(run), '.pdf'))
q <- do.call(rbind, lapply(time[-3], FUN=function(t) {
		par(mfrow=c(2,2))
		e <- lapply(1:opts$genes, FUN=function(i) {
			plot(35 + 58 * ddm[ddm$time == t,]$nuc/max(ddm[ddm$time == t,]$nuc), ddm[ddm$time == t, (gColumn + i)], type = 'l', col = 'red', main = paste('Gene', colnames(ddm)[(gColumn + i)], 'times', t), xlab = 'A-P', ylab = 'Conc', ylim = c(0, ylim_max))
			if (dim(wdata[wdata$time == t,])[1] > 0) {
				cr <- cor.test(ddm[ddm$time == t, (gColumn + i)], wdata[wdata$time == t, (opts$knn + i)])
				cat('average:', 'time:', t, 'gene:', i, 'cor:', cr$estimate, 'P:', cr$p.value, '\n')
			}
			qt <- lapply(run, FUN=function(j) {
				#cat(':', i,':',t,':' , j, length(ddm[ddm$time == t,]$nuc), length(dnpat[dnpat$time == t & dnpat$run == j, (gColumn + i)]), '\n')
				lines(35 + 58 * ddm[ddm$time == t,]$nuc/max(ddm[ddm$time == t,]$nuc), dnpat[dnpat$time == t & dnpat$run == j, (gColumn + i + 1)], type = 'l', col = cols[j + 1])
				if (dim(wdata[wdata$time == t,])[1] > 0) {
					cr <- cor.test(dnpat[dnpat$time == t & dnpat$run == j, (gColumn + i + 1)], wdata[wdata$time == t, (opts$knn + i)])
					cat('run:', j,'time:', t, 'gene:', i, 'cor:', cr$estimate, 'P:', cr$p.value, '\n')
				}
			})
			if (dim(wdata[wdata$time == t,])[1] > 0) {
				lines(35 + 58 * wdata[wdata$time == t,]$nuc/max(wdata[wdata$time == t,]$nuc), wdata[wdata$time == t, (opts$knn + i)]/wmax[opts$knn + i], type = 'l', col = 'blue')
			}
		})
	})
)

q <- do.call(rbind, lapply(time[-3], FUN=function(t) {
		par(mfrow=c(2,2))
		e <- lapply(1:opts$genes, FUN=function(i) {
			plot(35 + 58 * ddm[ddm$time == t,]$nuc/max(ddm[ddm$time == t,]$nuc), ddm[ddm$time == t, (mColumn + i)], type = 'l', col = 'red', main = paste('Gene', colnames(ddm)[(mColumn + i)], 'times', t), xlab = 'A-P', ylab = 'Conc', ylim = c(0, ylim_max))
			if (dim(wdata[wdata$time == t,])[1] > 0) {
				cr <- cor.test(ddm[ddm$time == t, (mColumn + i)], wdata[wdata$time == t, (opts$knn + i)])
				cat('average:', 'time:', t, 'gene:', i, 'cor:', cr$estimate, 'P:', cr$p.value, '\n')
			}
			qt <- lapply(run, FUN=function(j) {
				#cat(':', i,':',t,':' , j, length(ddm[ddm$time == t,]$nuc), length(dnpat[dnpat$time == t & dnpat$run == j, (gColumn + i)]), '\n')
				lines(35 + 58 * ddm[ddm$time == t,]$nuc/max(ddm[ddm$time == t,]$nuc), dnpat[dnpat$time == t & dnpat$run == j, (mColumn + i + 1)], type = 'l', col = cols[j + 1])
				if (dim(wdata[wdata$time == t,])[1] > 0) {
					cr <- cor.test(dnpat[dnpat$time == t & dnpat$run == j, (mColumn + i + 1)], wdata[wdata$time == t, (opts$knn + i)])
					cat('run:', j,'time:', t, 'gene:', i, 'cor:', cr$estimate, 'P:', cr$p.value, '\n')
				}
			})
			if (dim(wdata[wdata$time == t,])[1] > 0) {
				lines(35 + 58 * wdata[wdata$time == t,]$nuc/max(wdata[wdata$time == t,]$nuc), wdata[wdata$time == t, (opts$knn + i)]/wmax[opts$knn + i], type = 'l', col = 'blue')
			}
		})
	})
)

if (file.exists(logfile1)) {
	n_sites <- 125
	ulog <- read.csv(logfile1, header = F, sep=' ')
	cn <- c('repeat', 'kounter', 'slow', 'iter_kounter', 't_slow', 'tau_slow', 'reaction_number_slow', 'nuc_number_slow', 'promoter_number_slow', 'reaction_type_slow', 'fast', 'inner_iter_kounter', 'allele_0')
	sn <- paste0('s', seq(1:n_sites))
	sn
	g1sn <- paste0('g1', sn)
	g1sn
	Nmin <- opts$nuc
	Nmax <- 1
	if (opts$nuc == 0) {
		i <- opts$genes
		t <- time[(length(time) - 1)]
		Nmax <- ddm[ddm$time == t,]$nuc[which.max(ddm[ddm$time == t, (gColumn + i)])]
		Nmin <- ddm[ddm$time == t,]$nuc[which.min(ddm[ddm$time == t, (gColumn + i)])]
	}
	N <- Nmax
	par(mfrow=c(1,1))
	ulog <- ulog[,c(1:13,(13 + 125 * (N - 1) + 1):(13 + 125 * N))]
	dim(ulog)
	colnames(ulog) <- c(cn, g1sn)
	summary(ulog)

	#ulogN6 <- ulog[,c(1:13,(13 + 125 * 5 + 1):(13 + 125 * 6))]
	#colnames(ulogN6) <- c(cn,g1sn)
	#summary(ulogN6)

	ulog <- ulog[ulog$kounter>3,]
	#ulogN6 <- ulogN6[ulogN6$kounter>3,]

	#par(mfrow=c(1,2))
	hist(ulog[ulog$nuc_number_slow == (N - 1),]$reaction_type, xlab = 'Reaction type', main = paste0('Reactions at nuc ', N))
	#hist(ulogN6[ulogN6$nuc_number_slow == 5,]$reaction_type)

	ulogv <- split(ulog, ulog[,1])
	#ulogN6v <- split(ulogN6, ulogN6[,1])

	brks <- seq(0.5, 9.5, by = 1)
	brks
	#countsN6 <- do.call(rbind, lapply(ulogN6v, FUN=function(da) { hq <- hist(da[da$nuc_number_slow == 5,]$reaction_type_slow, breaks = brks, plot = F);return(hq$counts)}))
	counts <- do.call(rbind, lapply(ulogv, FUN=function(da) { hq <- hist(da[da$nuc_number_slow == (N - 1),]$reaction_type_slow, breaks = brks, plot = F);return(hq$counts)}))
	boxplot(counts, main = paste0('Reactions at nuc ', N))
	#boxplot(countsN6)

	colors <- c("blue", "green", "red", "black", "magenta", "violet", "grey", "lightblue", "darkblue", "yellow", "cyan", "brown", "lightgreen", "lightgrey")
	TFs <- c("hb", "Kr", "gt", "kni", "bcd", "cad", "tll", "hkb")

	sidat <- read.csv('sites.geq125.200.file', header = F, sep = ' ')
	colnames(sidat) <- c('x','strand','tf','en','len','pwm')

	countsts <- do.call(rbind, lapply(ulogv, FUN=function(da) { sapply(14:138, FUN=function(site) { return(sum(da[da[,site] == 1 & da$reaction_type_slow == 1,]$tau_slow)) })  }))
	countsts
	#countsN6ts <- do.call(rbind, lapply(ulogN6v, FUN=function(da) { sapply(14:138, FUN=function(site) { return(sum(da[da[,site] == 1 & da$reaction_type_slow == 1,]$tau_slow)) })  }))

	boxplot(countsts[,order(sidat$pwm)], col = colors[sidat$tf + 1], main = paste0('Time occupied in nuc ', N))
	#boxplot(countsN6ts, at = sidat$pwm, col = colors[sidat$tf + 1])

	#dev.off()
	#warnings()
	#q()

	tsmean <- colMeans(countsts)
	#ts6mean <- colMeans(countsN6ts)
	#cor.test(ts2mean, ts6mean)
	cor.test(sidat$en, tsmean)
	cor.test(sidat$pwm, tsmean)

	countstsHb <- countsts[,which(sidat$tf == 0)]
	countstsBcd <- countsts[,which(sidat$tf == 4)]

	bk <- seq(0, 1.5, by = 0.15)

	countstsHbcounts <- apply(countstsHb, 1, FUN=function(i) { hq <- hist(i, breaks = bk, plot = F); return(hq$counts) })
	countstsBcdcounts <- apply(countstsBcd, 1, FUN=function(i) { hq <- hist(i, breaks = bk, plot = F); return(hq$counts) })

	countstsHbcounts <- t(countstsHbcounts)
	countstsBcdcounts <- t(countstsBcdcounts)
	boxplot(countstsHbcounts, main = paste0('Time occupied Hb in nuc ', N))
	boxplot(countstsBcdcounts, main = paste0('Time occupied Bcd in nuc ', N))
}
dev.off()
warnings()
q()
