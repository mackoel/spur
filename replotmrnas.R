#!/usr/bin/Rscript --silent
rem <- function(...) invisible(T)
rem( '
Rscript.exe "%~F0" "%1" "%2" "%3" "%4" "%5" "%~F6" "%~F7","%~F8","%~F9"
EXIT /B
rem ')
### above=BAT, below=R
pkgrequire <- function(pkg, preferred = 'http://cran.rstudio.com/') {
	if (!require(pkg, character.only = TRUE)) {
		cat("Cannot find", pkg, "package.\n")
		lib_path = Sys.getenv("R_LIBS_USER")
		if (!file.exists(lib_path)) dir.create(lib_path, rec = TRUE)
		install.packages(pkg, dep = TRUE, repos = preferred, lib = lib_path)
		if (!require(pkg, character.only = TRUE, lib.loc = lib_path)) stop("Package not found")
	}
}
pkgrequire("optparse")
pkgrequire("RColorBrewer")
pkgrequire("png")

dotsplot1 <- function(step, conc1, maxbreaks1, mdots, lab = "circle", comp = "nuclei", obj = "Dots", fgc = "black", ordered = FALSE, dsize = 14, cright = TRUE, inclt = FALSE, doff = 1, boff = 1) {
	if (ordered == TRUE) {
		o1 = order(conc1)
		conc1 = conc1[o1]
		mdots1 = mdots[o1,]
	}
	nbreaks1 <- maxbreaks1 + 1
	if (nbreaks1 < 0) {
		brks1 <- unique(c(min(conc1), min(conc1) + boff, seq(min(conc1) + boff, max(conc1) + 1, by = (max(conc1) + 1 - min(conc1) - boff)/(-maxbreaks1) )))
		brks1 <- brks1[-c(length(brks1) - 1)]
		nbreaks1 = length(brks1) - 1
	} else if (nbreaks1 < 2) {
		brks1 <- 2
		nbreaks1 <- brks1
	} else if (nbreaks1 > 40) {
		brks1 <- 40
		nbreaks1 <- brks1
	} else {
		brks1 <- nbreaks1
	}
	if (step == 1) {
		palette1 <- colorRampPalette(c("grey", "blue", "red"))
	} else {
		palette1 <- colorRampPalette(c("grey", "yellow", "green"))
	}
	cutbreaks1 <- as.numeric(cut(conc1, breaks = brks1, labels = FALSE, right = cright, include.lowest = inclt))
	murs1 <- lapply(1:nbreaks1, FUN=function(i) {ind = which(cutbreaks1 == i); res = 0; if (length(ind) > 0) { res = max(conc1[ind]) }; return(res)} )
	mlrs1 <- lapply(1:nbreaks1, FUN=function(i) {ind = which(cutbreaks1 == i); res = 0; if (length(ind) > 0) { res = min(conc1[ind]) }; return(res)} )
	mlev1 <- paste(unlist(mlrs1), ":", unlist(murs1))
	palette <- palette1(nbreaks1)[cutbreaks1]
	layout(matrix(1:2, nrow = 1), widths = c(0.7, 0.3))
	opar <- par(mar = c(5.1, 4.1, 4.1, 2.1))
	radii <- (3 * mdots1$m000/ (4 *pi))^1/3
	mradii <- mean(radii)
	sradii <- sd(radii)
	koef <- 0.1
	radii[radii > (mradii + koef * sradii)] <- (mradii + koef * sradii)
	radii[radii < (mradii - koef * sradii)] <- (mradii - koef * sradii)
	if (lab == "circle") {
		symbols(x = mdots1$xmean, y = -mdots1$ymean, circles = radii, inches = 1/dsize, add = F, bg = palette, fg = fgc, xlab = 'X', ylab = 'Y', main = paste0(obj, ' number in ', comp, ' for Ch4 and Ch2'))
	} else {
		symbols(x = mdots1$xmean, y = -mdots1$ymean, squares = radii, inches = 1/dsize, add = F, bg = palette, fg = fgc, xlab = 'X', ylab = 'Y', main = paste0(obj, ' number in ', comp, ' for Ch4 and Ch2'))
	}
#
	xl <- 1
	yb <- 1
	xr <- 1.5
	yt <- 2
	par(opar, no.readonly = T)
	opar <- par(mar = c(5.1, 0.5, 4.1, 3.5))
	plot(NA, type = "n", ann = FALSE, xlim = c(1,2), ylim = c(1,2), xaxt = "n", yaxt = "n", bty = "n")
	rect(xl, head(seq(yb, yt, (yt - yb)/nbreaks1), -1),
	     xr, tail(seq(yb, yt, (yt - yb)/nbreaks1), -1),
	     col = palette1(nbreaks1))
	mtext(mlev1, side = 4, at = tail(seq(yb, yt, (yt - yb)/nbreaks1), -1) - 0.05, las = 2, cex = 0.7)
	par(opar, no.readonly = T)
}

dotsplot2 <- function(conc1, maxbreaks1, conc2, maxbreaks2, mdots, lab = "circle", comp = "nuclei", obj = "Dots", fgc = "black", ordered = FALSE, dsize = 14, cright = TRUE, inclt = FALSE, doff = 1, boff = 1, xxlim = c(0, 1024), yylim = c(0, 1024), imgbkg = "") {
	if (ordered == TRUE) {
		o1 = order(conc1)
		conc2 = conc2[o1]
		conc1 = conc1[o1]
		mdots1 = mdots[o1,]
	} else {
		o1 = order(mdots$zmean, decreasing=F)
		conc2 = conc2[o1]
		conc1 = conc1[o1]
		mdots1 = mdots[o1,]
	}
	nbreaks1 <- maxbreaks1 + 1
	if (nbreaks1 < 0) {
		brks1 <- unique(c(min(conc1), min(conc1) + boff, seq(min(conc1) + boff, max(conc1) + 1, by = (max(conc1) + 1 - min(conc1) - boff)/(-maxbreaks1) )))
		brks1 <- brks1[-c(length(brks1) - 1)]
		nbreaks1 = length(brks1) - 1
	} else if (nbreaks1 < 2) {
		brks1 <- 2
		nbreaks1 <- brks1
	} else if (nbreaks1 > 40) {
		brks1 <- 40
		nbreaks1 <- brks1
	} else {
		brks1 <- nbreaks1
	}
	palette1 <- colorRampPalette(c("grey", "blue", "red"))
	cutbreaks1 <- as.numeric(cut(conc1, breaks = brks1, labels = FALSE, right = cright, include.lowest = inclt))
#	cutbreaks <- as.numeric(cut(conc, breaks = c(0, 1,10,89,90), labels = FALSE, right = cright, include.lowest = inclt))
	murs1 <- lapply(1:nbreaks1, FUN=function(i) {ind = which(cutbreaks1 == i); res = 0; if (length(ind) > 0) { res = max(conc1[ind]) }; return(res)} )
	mlrs1 <- lapply(1:nbreaks1, FUN=function(i) {ind = which(cutbreaks1 == i); res = 0; if (length(ind) > 0) { res = min(conc1[ind]) }; return(res)} )
	mlev1 <- paste(floor(unlist(mlrs1)), ":", floor(unlist(murs1)))
	palette <- palette1(nbreaks1)[cutbreaks1]
#	layout(matrix(1:4, nrow = 2), widths = c(0.7, 0.3, 0.7, 0.3))
	layout(matrix(1:3, nrow = 1), widths = c(0.7, 0.15, 0.15))
	opar <- par(mar = c(5.1, 4.1, 4.1, 2.1))
	radii <- (3 * mdots1$m000/ (4 *pi))^1/3
	mradii <- mean(radii)
	sradii <- sd(radii)
	koef <- 0.1
#	cat('max:', max(radii), 'min:', min(radii), 'mean:', mradii, 'stdev:', sradii, 'f:', sum(radii > (mradii + koef * sradii)), '\n')
	radii[radii > (mradii + koef * sradii)] <- (mradii + koef * sradii)
	radii[radii < (mradii - koef * sradii)] <- (mradii - koef * sradii)
	addplot = F
#	"/storage2/kkozlov/Cloud@MailRu/Projects/EyeDisk/338-m6/MAX_-sch2-2.png"
	if (imgbkg != "") {
		ima <- readPNG(imgbkg)
		#Set up the plot area
		plot(x = mdots1$xmean, y = -mdots1$ymean, type='n', xlab = 'X', ylab = 'Y', main = paste0(obj, ' number in ', comp, ' for Ch2 and Ch4'), xlim = xxlim, ylim = yylim)	
		#Get the plot information so the image will fill the plot box, and draw it
		lim <- par()
		rasterImage(ima, xxlim[1], yylim[1], xxlim[2], yylim[2])
		addplot = T
	}
	if (lab == "circle") {
		symbols(x = mdots1$xmean, y = -mdots1$ymean, circles = radii, inches = 1/dsize, add = addplot, bg = palette, fg = fgc, xlab = 'X', ylab = 'Y', main = paste0(obj, ' number in ', comp, ' for Ch2 and Ch4'), xlim = xxlim, ylim = yylim)
	} else {
		symbols(x = mdots1$xmean, y = -mdots1$ymean, squares = radii, inches = 1/dsize, add = addplot, bg = palette, fg = fgc, xlab = 'X', ylab = 'Y', main = paste0(obj, ' number in ', comp, ' for Ch2 and Ch4'), xlim = xxlim, ylim = yylim)
	}
cat((unlist(murs1)[1] + 1), '\n')
	mlev2min = (unlist(murs1)[1] + 1)/doff
	u = (conc1 < conc2) & (mlev2min < conc2)
	conc2 = conc2[u]
	mdots2 = mdots1[u,]
	if (ordered == TRUE) {
		o2 = order(conc2)
		conc2 = conc2[o2]
		mdots2 = mdots2[o2,]
	}
	nbreaks2 <- maxbreaks2 + 1
	if (nbreaks2 < 0) {
		brks2 <- unique(c(min(conc2), min(conc2) + boff, seq(min(conc2) + boff, max(conc2) + 1, by = (max(conc2) + 1 - min(conc2) - boff)/(-maxbreaks2) )))
		brks2 <- brks2[-c(length(brks2) - 1)]
		nbreaks2 = length(brks2) - 1
	} else if (nbreaks2 < 2) {
		brks2 <- 2
		nbreaks2 <- brks2
	} else if (nbreaks2 > 40) {
		brks2 <- 40
		nbreaks2 <- brks2
	} else {
		brks2 <- nbreaks2
	}
	palette2 <- colorRampPalette(c("yellow", "green", "brown"))
	cutbreaks2 <- as.numeric(cut(conc2, breaks = brks2, labels = FALSE, right = cright, include.lowest = inclt))
	murs2 <- lapply(1:nbreaks2, FUN=function(i) {ind = which(cutbreaks2 == i); res = 0; if (length(ind) > 0) { res = max(conc2[ind]) }; return(res)} )
	mlrs2 <- lapply(1:nbreaks2, FUN=function(i) {ind = which(cutbreaks2 == i); res = 0; if (length(ind) > 0) { res = min(conc2[ind]) }; return(res)} )
	mlev2 <- paste(floor(unlist(mlrs2)), ":", floor(unlist(murs2)))
	palette <- palette2(nbreaks2)[cutbreaks2]
#
	opar <- par(mar = c(5.1, 4.1, 4.1, 2.1))
	radii <- (3 * mdots2$m000/ (4 *pi))^1/3
	mradii <- mean(radii)
	sradii <- sd(radii)
	koef <- 0.1
	radii[radii > (mradii + koef * sradii)] <- (mradii + koef * sradii)
	radii[radii < (mradii - koef * sradii)] <- (mradii - koef * sradii)
	if (lab == "circle") {
		symbols(x = mdots2$xmean, y = -mdots2$ymean, circles = radii, inches = 1/dsize, add = T, bg = palette, fg = fgc, xlab = 'X', ylab = 'Y')
	} else {
		symbols(x = mdots2$xmean, y = -mdots2$ymean, squares = radii, inches = 1/dsize, add = T, bg = palette, fg = fgc, xlab = 'X', ylab = 'Y')
	}
#
	xl <- 1
	yb <- 1
	xr <- 1.5
	yt <- 2
	par(opar, no.readonly = T)
	opar <- par(mar = c(5.1, 0.5, 4.1, 3.5))
	plot(NA, type = "n", ann = FALSE, xlim = c(1,2), ylim = c(1,2), xaxt = "n", yaxt = "n", bty = "n")
	rect(xl, head(seq(yb, yt, (yt - yb)/nbreaks1), -1),
	     xr, tail(seq(yb, yt, (yt - yb)/nbreaks1), -1),
	     col = palette1(nbreaks1))
	mtext(mlev1, side = 4, at = tail(seq(yb, yt, (yt - yb)/nbreaks1), -1) - 0.05, las = 2, cex = 0.7)
	par(opar, no.readonly = T)
#
	xl <- 1
	yb <- 1
	xr <- 1.5
	yt <- 2
	par(opar, no.readonly = T)
	opar <- par(mar = c(5.1, 0.5, 4.1, 3.5))
	palette = c('grey', palette2(nbreaks2))
	nbreaks2 = nbreaks2 + 1
	mlev2 = c(paste("0:", floor(mlev2min)), mlev2)
	plot(NA, type = "n", ann = FALSE, xlim = c(1,2), ylim = c(1,2), xaxt = "n", yaxt = "n", bty = "n")
	rect(xl, head(seq(yb, yt, (yt - yb)/nbreaks2), -1),
	     xr, tail(seq(yb, yt, (yt - yb)/nbreaks2), -1),
	     col = palette)
	mtext(mlev2, side = 4, at = tail(seq(yb, yt, (yt - yb)/nbreaks2), -1) - 0.05, las = 2, cex = 0.7)
	par(opar, no.readonly = T)
}

#dotsplot2(anuc[,]$ch4_mrnas, 4, anuc[,]$ch2_mrnas, 6, anuc[,], lab = 'circle', comp = 'cells', obj = "mrnas", fgc = "white", ordered = T, dsize = 30, cright = F)

option_list <- list(
	make_option(c("-v", "--verbose"), action="store_true", default=FALSE,
							help="Print extra output [default=false]"),
	make_option(c("-a", "--minint"), type="double", default = 0,
							help="minint [default %default]"),
	make_option(c("-b", "--breaks"), type="integer", default = 50,
							help="breaks [default %default]", metavar="number"),
	make_option(c("-c", "--comp"), type="character", default = "mrna",
							help="comp [default %default]"),
	make_option(c("-d", "--doff"), type="double", default = 1,
							help="doff [default %default]", metavar="number"),
	make_option(c("-l", "--lab"), type="character", default = "circle",
							help="lab [default %default]"),
	make_option(c("-k", "--imgbkg"), type="character", default = "",
							help="imgbkg [default empty %default]"),
	make_option(c("-m", "--lim"), type="integer", default = 30,
							help="lim [default %default]"),
	make_option(c("-n", "--name"), type="character", default = "mRNAs",
							help="name [default %default]"),
	make_option(c("-o", "--ordered"), action="store_true", default=FALSE,
							help="order by conc [default=false]"),
	make_option(c("-p", "--pdf"), action="store_true", default=FALSE,
							help="PDF [default=false]"),
	make_option(c("-i", "--x1"), type="integer", default = 3,
							help="x0 [default %default]", metavar="number"),
	make_option(c("-j", "--y1"), type="integer", default = 2,
							help="y0 [default %default]", metavar="number"),
	make_option(c("-t", "--tag"), type="character", default = "overlay",
							help="smooth [default %default]")
)
opt_parser <- OptionParser(usage = "usage: %prog [options]", option_list = option_list, description = "Plot cells filtered in different way", epilogue = "Send feedback to mackoel@gmail.com")
# postplotnuc.R -b 100 -m 100 -t cref -n ch1 -1-qmd-1.csv,-qcell.csv -1-cplot.pdf,-qcell-corr.csv,-1-qmd-corr.csv
cmdline <- commandArgs(trailingOnly = TRUE)

flaggs <- parse_args(opt_parser, args = cmdline, print_help_and_exit = TRUE, positional_arguments = TRUE)
opts <- flaggs$options
args <- flaggs$args

inpfiles <- unlist(strsplit(args[1], ",", fixed = TRUE))
atabfile <- inpfiles[1]
outfiles <- unlist(strsplit(args[2], ",", fixed = TRUE))
flatresfile <-  outfiles[1]

anuc <- read.csv(atabfile)
#d1 = mean(anuc$zmean)
#d2 = sd(anuc$zmean)
#anuc$zmean = abs(anuc$zmean - mean(anuc$zmean))
#anuc = anuc[(anuc$zmean > d1 - 0.2 * d2) & (anuc$zmean < d1 + 0.2 * d2), ]
xxlim <- c(min(anuc$xmean), max(anuc$xmean))
yylim <- c(-max(anuc$ymean), -min(anuc$ymean))
anuc = anuc[(anuc$ch1_mrnas + anuc$ch2_mrnas + anuc$ch3_mrnas + anuc$ch4_mrnas) > opts$minint, ]
if (opts$pdf == TRUE) {
	pdf(file = flatresfile, width = 10)
} else {
	png(file = flatresfile, width = 1200, heigh = 720)
}
if (opts$tag == "overlay") {
#dotsplot(q, max(q), anuc, lab = opts$lab, comp = opts$comp, obj = "mrnas")
	if (opts$comp == "mrna") {
		dotsplot2(anuc[,]$ch2_mrnas, opts$x1, anuc[,]$ch4_mrnas, opts$y1, anuc[,], lab = opts$lab, comp = opts$comp, obj = opts$name, fgc = "white", ordered = opts$ordered, dsize = opts$lim, cright = F, doff = opts$doff, boff = opts$breaks, xxlim = xxlim, yylim = yylim, imgbkg = opts$imgbkg)
	} else {
		dotsplot2(anuc[,]$cell2_mean, opts$x1, anuc[,]$cell4_mean, opts$y1, anuc[,], lab = opts$lab, comp = 'cells', obj = opts$name, fgc = "white", ordered = opts$ordered, dsize = opts$lim, cright = F, doff = opts$doff, boff = opts$breaks, xxlim = xxlim, yylim = yylim, imgbkg = opts$imgbkg)
	}
} else {
	dotsplot1(1, anuc[,]$ch2_mrnas, opts$x1, anuc[,], lab = opts$lab, comp = 'cells', obj = opts$name, fgc = "white", ordered = opts$ordered, dsize = opts$lim, cright = F, doff = 1, boff = opts$breaks)
	dotsplot1(2, anuc[,]$ch4_mrnas, opts$y1, anuc[,], lab = opts$lab, comp = 'cells', obj = opts$name, fgc = "white", ordered = opts$ordered, dsize = opts$lim, cright = F, doff = opts$doff, boff = opts$breaks)
}

qq <- dev.off()

