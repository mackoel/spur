#include <fann.h>

/* compile
 * gcc -o fann_train -I/usr/include -L/usr/lib64 -lfann -lm fann_train.c
 * run
 * ./fann_train
 */

int main()
{
    const unsigned int num_input = 3248;
    const unsigned int num_output = 1;
    const unsigned int num_layers_hidden = 3;
    unsigned int num_layers;
    unsigned int *num_neurons_hidden;
    const float desired_error = (const float) 0.001;
    const unsigned int max_epochs = 500000;
    const unsigned int epochs_between_reports = 1000;

    num_layers = num_layers_hidden + 2;

    num_neurons_hidden = (unsigned int *)malloc(num_layers * sizeof(unsigned int *));
    num_neurons_hidden[0] = num_input;
    num_neurons_hidden[1] = 20;
    num_neurons_hidden[2] = 5;
    num_neurons_hidden[3] = 3;
    num_neurons_hidden[4] = num_output;

    struct fann *ann = fann_create_standard_array(num_layers, num_neurons_hidden);

    fann_set_activation_function_hidden(ann, FANN_SIGMOID_SYMMETRIC);
    fann_set_activation_function_output(ann, FANN_SIGMOID_SYMMETRIC);

    fann_train_on_file(ann, "xor.data", max_epochs,
        epochs_between_reports, desired_error);

    fann_save(ann, "xor_float.net");

    fann_destroy(ann);

    return 0;
}
