#!/usr/bin/Rscript --silent
rem <- function(...) invisible(T)
rem( '
Rscript.exe "%~F0" "%1" "%2" "%3" "%4" "%5" "%~F6" "%~F7","%~F8","%~F9"
EXIT /B
rem ')
### above=BAT, below=R
pkgrequire <- function(pkg, preferred = 'http://cran.rstudio.com/') {
	if (!require(pkg, character.only = TRUE)) {
		cat("Cannot find", pkg, "package.\n")
		lib_path = Sys.getenv("R_LIBS_USER")
		if (!file.exists(lib_path)) dir.create(lib_path, rec = TRUE)
		install.packages(pkg, dep = TRUE, repos = preferred, lib = lib_path)
		if (!require(pkg, character.only = TRUE, lib.loc = lib_path)) stop("Package not found")
	}
}
pkgrequire("optparse")
pkgrequire("RColorBrewer")
pkgrequire("plotrix")

palette_func <- colorRampPalette(c("grey", "yellow", "red", "brown", "darkorchid4", "darkblue") )

hexcellplot <- function(rhos, phis, qounts, qmin, qnbrks, qmax, qntcll = 0, doff = "", cright = TRUE, inclt = FALSE, conv2percent = TRUE, ...) {
	o1 = order(qounts)
	qunitcell = qntcll
	if (conv2percent) {
		qphi <- 100 * (phis - min(phis))/diff(range(phis))
		qrho <- 100 - 100 * (rhos - min(rhos))/diff(range(rhos))
		if (qntcll == 0) {
			qunitcell = 0.95
		}
	} else {
		qphi <- phis
		qrho <- -rhos
		if (qntcll == 0) {
			qunitcell = 10
		}
		#qunitcell = 0.95
	}
	qounts = qounts[o1]
	qphi = qphi[o1]
	qrho = qrho[o1]
	if (qnbrks < 0) {
		brks <- unique(c(0, qmin, seq(qmin, max(qounts) - qmax, by = (max(qounts) - qmax - qmin)/(-qnbrks)), max(qounts)))
		nbreaks = length(brks) - 1
	} else if (qnbrks < 2) {
		brks <- 2
		nbreaks <- brks
	} else if (qnbrks > 40) {
		brks <- 40
		nbreaks <- brks
	} else {
		nbreaks <- qnbrks
		brks <- nbreaks
	}
	cutbreaks <- as.numeric(cut(qounts, breaks = brks, labels = FALSE, right = cright, include.lowest = inclt))
	murs <- lapply(1:nbreaks, FUN=function(i) {ind = which(cutbreaks == i); res = 0; if (length(ind) > 0) { res = max(qounts[ind]) }; return(res)} )
	mlrs <- lapply(1:nbreaks, FUN=function(i) {ind = which(cutbreaks == i); res = 0; if (length(ind) > 0) { res = min(qounts[ind]) }; return(res)} )
	mlev <- paste(unlist(mlrs), ":", unlist(murs))
	colpal <- paste(palette_func(nbreaks)[cutbreaks], doff, sep = '')
	layout(matrix(1:2, nrow = 1), widths = c(0.7, 0.3))
	opar <- par(mar = c(5.1, 4.1, 4.1, 2.1))
	plot(0, 0, type = "n", xlim = range(qphi), ylim = range(qrho), asp = 1, ...)
	QQ <- lapply(1:length(qounts), FUN=function(i) {
		hexagon(qphi[i], qrho[i], col = colpal[i], unitcell = qunitcell, border = "black")
	})
	xl <- 1
	yb <- 1
	xr <- 1.5
	yt <- 2
	par(opar, no.readonly = T)
	opar <- par(mar = c(5.1, 0.5, 4.1, 3.5))
	plot(NA, type = "n", ann = FALSE, xlim = c(1,2), ylim = c(1,2), xaxt = "n", yaxt = "n", bty = "n")
	rect(xl, head(seq(yb, yt, (yt - yb)/nbreaks), -1),
	     xr, tail(seq(yb, yt, (yt - yb)/nbreaks), -1),
	     col = palette_func(nbreaks))
	mtext(mlev, side = 4, at = tail(seq(yb, yt, (yt - yb)/nbreaks), -1) - 0.05, las = 2, cex = 0.7)
	par(opar, no.readonly = T)
}

option_list <- list(
	make_option(c("-v", "--verbose"), action="store_true", default=FALSE,
							help="Print extra output [default=false]"),
	make_option(c("-C", "--conv2pc"), action="store_false", default=TRUE,
							help="convert to percent [default=true]"),
	make_option(c("-a", "--minint"), type="double", default = 10,
							help="Number of breaks for final ato pattern [default %default]"),
	make_option(c("-b", "--breaks"), type="integer", default = 50,
							help="Zero level for raw hex pattern [default %default]", metavar="number"),
	make_option(c("-r", "--rev"), type="integer", default = 10,
							help="Zero level for final ato pattern [default %default]"),
	make_option(c("-i", "--x1"), type="integer", default = 3,
							help="Number of breaks for raw hex pattern [default %default]", metavar="number")
)
opt_parser <- OptionParser(usage = "usage: %prog [options]", option_list = option_list, description = "Plot cells filtered in different way", epilogue = "Send feedback to mackoel@gmail.com")
# postplotnuc.R -b 100 -m 100 -t cref -n ch1 -1-qmd-1.csv,-qcell.csv -1-cplot.pdf,-qcell-corr.csv,-1-qmd-corr.csv
cmdline <- commandArgs(trailingOnly = TRUE)

#../../../../Cloud@MailRu/Projects/EyeDisk/plotcountshex.R -j 55 -i -15 -b -40 -k max_shc2.png -qcell-corr.csv,-1-qmd-corr.csv,-2-qmd-corr.csv,-3-qmd-corr.csv,-4-qmd-corr.csv -m 25 -p g1.pdf

flaggs <- parse_args(opt_parser, args = cmdline, print_help_and_exit = TRUE, positional_arguments = TRUE)
opts <- flaggs$options
args <- flaggs$args

inpfiles <- unlist(strsplit(args[1], ",", fixed = TRUE))
atabfile <- inpfiles[1]

outfiles <- unlist(strsplit(args[2], ",", fixed = TRUE))
flatresfile <-  outfiles[1]

model_data <- read.csv(atabfile)

# model_data:
# xmean = cell D-V coordinate
# ymean = cell A-P coordinate
# m000 = number of particles in 4 channels
# ch1_mrnas, ch2_mrnas, ch3_mrnas, ch4_mrnas - number of mrnas in the channels
# mfc = differentiated
# ommatidia = atonal cells greater than neighbours
# th = lowest atonal
# ncols = number of columns in the image
# nrows = number of rows in the image
# ant = border between eye and antenna
# real_hh = hedgehog cells greater than neighbours
# ch1_mrnas_corr - number of hairy mrnas corrected using the fact that hairy is expressed only before MF
# ch3_mrnas_corr - number of delta mrnas corrected using linear regression on atonal number of mrnas
# mf = cells in the MF
# rho = polar coordinate rho
# phi = polar coordinate phi
# ax = x coordinate of the pole
# ay = y coordinate of the pole
# radius = radius of the MF
# sprho = distance to the spline approximated MF
# spphi = coordinate along spline approximated MF

#wd <- getwd()

#individ <- basename(wd)
#wd<- dirname(wd)
#genotype <- basename(wd)
#outputfile <- paste(genotype, individ, '2d-qj.csv', sep = '-')

pdf(file = flatresfile, width = 10)
par(mfrow=c(1,1))

scount <- model_data$ch1_mrnas
hexcellplot(model_data$ymean, model_data$xmean, scount, opts$breaks, opts$x1, 0, conv2percent = opts$conv2pc, xlab='y, D-V', ylab='x, A-P', main='raw hairy')
hexcellplot(model_data$rho, model_data$phi, scount, opts$breaks, opts$x1, 0, conv2percent = opts$conv2pc, xlab='phi, D-V', ylab='rho, A-P', main='raw hairy - circle')
hexcellplot(model_data$sprho, model_data$spphi, scount, opts$breaks, opts$x1, 0, conv2percent = opts$conv2pc, xlab='spphi, D-V', ylab='sprho, A-P', main='raw hairy - spline')
hexcellplot(model_data$row_id, model_data$col_id, scount, opts$breaks, opts$x1, 0, qntcll = 0.85, conv2percent = FALSE, xlab='col id, D-V', ylab='row id, A-P', main='raw hairy - id')
scount <- model_data$ch1_mrnas_corr
hexcellplot(model_data$ymean, model_data$xmean, scount, opts$breaks, opts$x1, 0, conv2percent = opts$conv2pc, xlab='y, D-V', ylab='x, A-P', main='corrected hairy')
hexcellplot(model_data$rho, model_data$phi, scount, opts$breaks, opts$x1, 0, conv2percent = opts$conv2pc, xlab='phi, D-V', ylab='rho, A-P', main='corrected hairy - circle')
hexcellplot(model_data$sprho, model_data$spphi, scount, opts$breaks, opts$x1, 0, conv2percent = opts$conv2pc, xlab='spphi, D-V', ylab='sprho, A-P', main='corrected hairy - spline')
hexcellplot(model_data$row_id, model_data$col_id, scount, opts$breaks, opts$x1, 0, qntcll = 0.85, conv2percent = FALSE, xlab='col id, D-V', ylab='row id, A-P', main='corrected hairy - id')

scount <- model_data$ch2_mrnas
hexcellplot(model_data$ymean, model_data$xmean, scount, opts$rev, opts$minint, 0, conv2percent = opts$conv2pc, xlab='y, D-V', ylab='x, A-P', main='raw ato')
hexcellplot(model_data$rho, model_data$phi, scount, opts$rev, opts$minint, 0, conv2percent = opts$conv2pc, xlab='phi, D-V', ylab='rho, A-P', main='raw ato - circle')
hexcellplot(model_data$sprho, model_data$spphi, scount, opts$rev, opts$minint, 0, conv2percent = opts$conv2pc, xlab='spphi, D-V', ylab='sprho, A-P', main='raw ato - spline')
hexcellplot(model_data$row_id, model_data$col_id, scount, opts$rev, opts$minint, 0, qntcll = 0.85, conv2percent = FALSE, xlab='col id, D-V', ylab='row id, A-P', main='raw ato - id')
#scount[!model_data$mfc & !model_data$mf] <- 0
#scount[!model_data$ommatidia & !model_data$mf] <- 0
scount <- model_data$ch2_mrnas_corr
hexcellplot(model_data$ymean, model_data$xmean, scount, opts$rev, opts$minint, 0, conv2percent = opts$conv2pc, xlab='y, D-V', ylab='x, A-P', main='corrected ato')
hexcellplot(model_data$rho, model_data$phi, scount, opts$rev, opts$minint, 0, conv2percent = opts$conv2pc, xlab='phi, D-V', ylab='rho, A-P', main='corrected ato - circle')
hexcellplot(model_data$sprho, model_data$spphi, scount, opts$rev, opts$minint, 0, conv2percent = opts$conv2pc, xlab='spphi, D-V', ylab='sprho, A-P', main='corrected ato - spline')
hexcellplot(model_data$row_id, model_data$col_id, scount, opts$rev, opts$minint, 0, qntcll = 0.85, conv2percent = FALSE, xlab='col id, D-V', ylab='row id, A-P', main='corrected ato - id')

scount <- model_data$ch3_mrnas
hexcellplot(model_data$ymean, model_data$xmean, scount, opts$breaks, opts$x1, 0, conv2percent = opts$conv2pc, xlab='y, D-V', ylab='x, A-P', main='raw delta')
hexcellplot(model_data$rho, model_data$phi, scount, opts$breaks, opts$x1, 0, conv2percent = opts$conv2pc, xlab='phi, D-V', ylab='rho, A-P', main='raw delta - circle')
hexcellplot(model_data$sprho, model_data$spphi, scount, opts$breaks, opts$x1, 0, conv2percent = opts$conv2pc, xlab='spphi, D-V', ylab='sprho, A-P', main='raw delta - spline')
hexcellplot(model_data$row_id, model_data$col_id, scount, opts$breaks, opts$x1, 0, qntcll = 0.85, conv2percent = FALSE, xlab='col id, D-V', ylab='row id, A-P', main='raw delta - id')
scount <- model_data$ch3_mrnas_corr
hexcellplot(model_data$ymean, model_data$xmean, scount, opts$breaks, opts$x1, 0, conv2percent = opts$conv2pc, xlab='y, D-V', ylab='x, A-P', main='corrected delta')
hexcellplot(model_data$rho, model_data$phi, scount, opts$breaks, opts$x1, 0, conv2percent = opts$conv2pc, xlab='phi, D-V', ylab='rho, A-P', main='corrected delta - circle')
hexcellplot(model_data$sprho, model_data$spphi, scount, opts$breaks, opts$x1, 0, conv2percent = opts$conv2pc, xlab='spphi, D-V', ylab='sprho, A-P', main='corrected delta - spline')
hexcellplot(model_data$row_id, model_data$col_id, scount, opts$breaks, opts$x1, 0, qntcll = 0.85, conv2percent = FALSE, xlab='col id, D-V', ylab='row id, A-P', main='corrected delta - id')

scount <- model_data$ch4_mrnas
hexcellplot(model_data$ymean, model_data$xmean, scount, opts$breaks, opts$x1, 0, conv2percent = opts$conv2pc, xlab='y, D-V', ylab='x, A-P', main='raw hedgehog')
hexcellplot(model_data$rho, model_data$phi, scount, opts$breaks, opts$x1, 0, conv2percent = opts$conv2pc, xlab='phi, D-V', ylab='rho, A-P', main='raw hedgehog - circle')
hexcellplot(model_data$sprho, model_data$spphi, scount, opts$breaks, opts$x1, 0, conv2percent = opts$conv2pc, xlab='spphi, D-V', ylab='sprho, A-P', main='raw hedgehog - spline')
hexcellplot(model_data$row_id, model_data$col_id, scount, opts$breaks, opts$x1, 0, qntcll = 0.85, conv2percent = FALSE, xlab='col id, D-V', ylab='row id, A-P', main='raw hedgehog - id')
#scount[!model_data$mfc & !model_data$mf] <- 0
#scount[model_data$mfc & !model_data$real_hh] <- 0
scount <- model_data$ch4_mrnas_corr
hexcellplot(model_data$ymean, model_data$xmean, scount, opts$breaks, opts$x1, 0, conv2percent = opts$conv2pc, xlab='y, D-V', ylab='x, A-P', main='corrected hedgehog')
hexcellplot(model_data$rho, model_data$phi, scount, opts$breaks, opts$x1, 0, conv2percent = opts$conv2pc, xlab='phi, D-V', ylab='rho, A-P', main='corrected hedgehog - circle')
hexcellplot(model_data$sprho, model_data$spphi, scount, opts$breaks, opts$x1, 0, conv2percent = opts$conv2pc, xlab='spphi, D-V', ylab='sprho, A-P', main='corrected hedgehog - spline')
hexcellplot(model_data$row_id, model_data$col_id, scount, opts$breaks, opts$x1, 0, qntcll = 0.85, conv2percent = FALSE, xlab='col id, D-V', ylab='row id, A-P', main='corrected hedgehog -id')

qq <- dev.off()

