#!/usr/bin/Rscript --silent

options(digits=22)

library(RColorBrewer)
library(gplots)

jfunc <- function(tfbs, paramfile, basetab) {
#	uofile <- gsub("\\s+", ".", paste0(paramfile, " ", j, " ", genes[k], " ", text[j], ".uof"))
	uofile <- gsub("\\s+", ".", paste0(paramfile, " ", tfbs[1], " ", tfbs[2], " ", tfbs[3], " ", tfbs[4], " ", tfbs[5], ".uof"))
	currtab <- read.csv(uofile, header = F, skip = 2, stringsAsFactors = F, sep = "", blank.lines.skip = TRUE, nrows = 494)
	currtab <- currtab[c(-1, -2)]
	return(currtab-basetab)
}

paramfile <- "D_con.429j.6.ini-deep-output"
baseuof <- "D_con.429j.6.ini-deep-output.uof"
sitesfilelog <- "log-sites-_con.429j.6-base.csv"

args <- commandArgs(trailingOnly = TRUE)
if ( length(args) > 0 ) {
	paramfile <- args[1]
}
if ( length(args) > 1 ) {
	baseuof <- args[2]
}
if ( length(args) > 2 ) {
	sitesfilelog <- args[3]
}

shortdat <- read.csv(file = sitesfilelog, stringsAsFactors = F, row.names=1)
basetab <- read.csv(baseuof, header = F, skip = 2, stringsAsFactors = F, sep = "", blank.lines.skip = TRUE, nrows = 494)

basetab <- basetab[c(-1, -2)]

Nexp <- dim(shortdat)[1]
Nexp

fulltab <- do.call('cbind', apply(shortdat, 1, FUN=jfunc, paramfile = paramfile, basetab = basetab))

fulltab <- as.matrix(fulltab)
dim(fulltab) <- c(494, 8, Nexp)
save(fulltab, file=paste0(paramfile, "-fulltab.Rdata"))

sitescorrtab <- do.call("cbind", lapply(1:Nexp, FUN=function(j) {
								do.call("rbind", lapply(1:Nexp, FUN=function(i) { return(cor(as.vector(fulltab[,,i]), as.vector(fulltab[,,j]))) } ))
							} ))

rownames(sitescorrtab) <- paste0(shortdat[,1], ".", shortdat[,2], ".", shortdat[,3], ".", shortdat[,5])
save(sitescorrtab, file=paste0(paramfile, "-sitescorrtab.Rdata"))

my_palette <- colorRampPalette(c("red", "blue", "yellow"))(n = 299)

mtab <- fulltab[1:10,,1:10] 
mcorrtab <- do.call("cbind", lapply(1:10, FUN=function(j) {
								do.call("rbind", lapply(1:10, FUN=function(i) { return(cor(as.vector(mtab[,,i]), as.vector(mtab[,,j]))) } ))
							} ))
							
rownames(mcorrtab) <- paste0(shortdat[1:10,1], ".", shortdat[1:10,2], ".", shortdat[1:10,3], ".", shortdat[1:10,5])
mcorrtab <- mcorrtab[order(shortdat[1:10,]$target, shortdat[1:10,]$TF),order(shortdat[1:10,]$target, shortdat[1:10,]$TF)]

# bcd,cad,gt,hb,hkb,kni,Kr,tll

sitescorrtab <- sitescorrtab[order(shortdat$target, shortdat$TF),order(shortdat$target, shortdat$TF)]
pdf(file=paste0(paramfile, "-hmap.pdf"))

heatmap.2(mcorrtab,
  main = "Correlation", # heat map title
  density.info="none",  # turns off density plot inside color legend
  trace="none",         # turns off trace lines inside the heat map
  col=my_palette,       # use on color palette defined earlier 
  dendrogram = "none",
  margins = c(5,10),
  colsep = c(369, 696, 1059),
  rowsep = c(49, 93, 129, 240, 262, 295, 339, 269, 407, 442, 472, 597, 613, 641, 668, 696, 732, 784, 810, 951, 965, 1004, 1027, 1059, 1089, 1137, 1174, 1307, 1314, 1345, 1377),
  sepcolor = "black",
  sepwidth = c(0.07, 0.07),
  Rowv="NA",
  Colv="NA")            # turn off column clustering

heatmap.2(sitescorrtab,
  main = "Correlation", # heat map title
  density.info="none",  # turns off density plot inside color legend
  trace="none",         # turns off trace lines inside the heat map
  col=my_palette,       # use on color palette defined earlier 
  dendrogram = "none",
  margins = c(5,10),
  colsep = c(369, 696, 1059),
  rowsep = c(49, 93, 129, 240, 262, 295, 339, 269, 407, 442, 472, 597, 613, 641, 668, 696, 732, 784, 810, 951, 965, 1004, 1027, 1059, 1089, 1137, 1174, 1307, 1314, 1345, 1377),
  sepcolor = "black",
  sepwidth = c(0.07, 0.07),
  Rowv="NA",
  Colv="NA")            # turn off column clustering
 
dev.off()

